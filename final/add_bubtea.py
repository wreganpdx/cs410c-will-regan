from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import btmodel

class Add_Bubtea(MethodView):
    def get(self):
        return render_template('add_bubtea.html')

    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed.
        """
        model = btmodel.get_model()
        model.insert(request.form['name'], request.form['address'], request.form['phone'])
        return redirect(url_for('index'))
