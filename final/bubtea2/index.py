from flask import render_template
from flask.views import MethodView
import gbmodel
import btmodel

class Index(MethodView):
    def get(self):
        model = gbmodel.get_model()
        b_model = btmodel.get_model()
        bt_entries = [dict(name=row[0], address=row[1], signed_on=row[2], phone=row[3] ) for row in b_model.select()]
        entries = [dict(name=row[0], email=row[1], signed_on=row[2], message=row[3] ) for row in model.select()]
        return render_template('index.html',entries=entries, bt_entries=bt_entries)
