"""
A simple Store to Drink database for a flask app.
ata is stored in a SQLite database that looks something like the following:

+------------+---------------
| Drink       | Labels    |
+============+================
| Mega Bubble  | Description |
+------------+-----------------

This can be created with the following SQL (see bottom of this file):

    create table book (store text, drink text);

"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'label_entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from labels_rel")
        except sqlite3.OperationalError:
            cursor.execute("create table labels_rel (label text, drink text, lang text)")
        cursor.close()

    def select(self, lang):
        """
        Gets all rows from the database
        Each row contains: store, drink
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT distinct * FROM labels_rel where lang='" + lang +"'")
        return cursor.fetchall()

    def get_drink(self, drink):
        """
        Gets specific store from database
        :return: single tuple from specific store (found by name)
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        search = "SELECT * FROM labels_rel where drink ='" + drink+"'"
        cursor.execute(search)
        return cursor.fetchall()

    def insert(self, label, drink, lang):
        """
        Inserts entry into database
        :param label: String
        :param drink: String
	:param lang: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'label':label, 'drink':drink, 'lang':lang}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into labels_rel (label, drink, lang) VALUES (:label, :drink, :lang)", params)

        connection.commit()
        cursor.close()
        return True
