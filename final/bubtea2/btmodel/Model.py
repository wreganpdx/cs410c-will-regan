class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def get_store(self,store):
        """
        Gets specific store from database
        :return: single tuple from specific store (found by name)
        """
        pass

    def insert(self, name, address, phone):
        """
        Inserts entry into database
        :param name: String
        :param email: String
        :param message: String
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass
