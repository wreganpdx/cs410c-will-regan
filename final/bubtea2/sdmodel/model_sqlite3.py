"""
A simple Store to Drink database for a flask app.
ata is stored in a SQLite database that looks something like the following:

+------------+---------------
| Store       | Drink    |
+============+================
| Mega Bubble  | Red Berry |
+------------+-----------------

This can be created with the following SQL (see bottom of this file):

    create table book (store text, drink text);

"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'drink_entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from drinks_rel")
        except sqlite3.OperationalError:
            cursor.execute("create table drinks_rel (store text, drink text)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: store, drink
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM drinks_rel")
        return cursor.fetchall()

    def get_store(self, store):
        """
        Gets specific store from database
        :return: single tuple from specific store (found by name)
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        search = "SELECT * FROM drinks_rel where store ='" + store+"'"
        cursor.execute(search)
        return cursor.fetchall()

    def insert(self, store, drink):
        """
        Inserts entry into database
        :param store: String
        :param drink: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'store':store, 'drink':drink}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into drinks_rel (store, drink) VALUES (:store, :drink)", params)

        connection.commit()
        cursor.close()
        return True
