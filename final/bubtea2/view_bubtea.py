from flask import render_template
from flask.views import MethodView
import btmodel
import sdmodel
import dlmodel
"""
This model will display the address, phone number and drinks available at a bubble tea store.
"""
class View_BubTea(MethodView):
    def get(self, bub, lang):
        sd_model = sdmodel.get_model()
        bt_model = btmodel.get_model()
        dl_model = dlmodel.get_model()
        
        bt_entries = [dict(name=row[0], address=row[1], signed_on=row[2], phone=row[3] ) for row in bt_model.get_store(bub)]
        sd_entries = [dict(store=row[0], drink=row[1]) for row in sd_model.get_store(bub)]
        dl_entries = [dict(label=row[0], drink=row[1]) for row in dl_model.select(lang)]
        return render_template('view_bubtea.html',sd_entries=sd_entries, bt_entries=bt_entries, dl_entries=dl_entries)
