from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import dimodel
import shutil
import requests
import dlmodel
import dimodel
from google.cloud import vision
from google.cloud import translate_v2 as translate
import six
"""
Add image will add to a drink-image combination to a relational database.
"""
class Add_Image(MethodView):
    def get(self, drink):
        return render_template('add_image.html', drink=drink)

    def post(self, drink):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed.
        """
        model = dimodel.get_model()
        label_model = dlmodel.get_model()
        url = request.form['url']
        client = vision.ImageAnnotatorClient()
        image = vision.types.Image()
        image.source.image_uri = url
        resp = client.label_detection(image=image)
        labels = resp.label_annotations
        to_string = ""
        print('Labels:')

        translate_client = translate.Client()

        for label in labels:
            print(label.description)
            t = label.description
            if isinstance(t, six.binary_type):
                t = t.decode('utf-8')
            result = translate_client.translate(t, 'es')
            label_model.insert(label.description, drink, 'eng')
            label_model.insert(u'{}'.format(result['translatedText']), drink, 'esp')
        

        
        
        response = requests.get(url, stream=True)
        strFile = drink + '.png'
        strFileLoc = 'static/' + strFile
        with open(strFileLoc, 'wb') as out_file:
                shutil.copyfileobj(response.raw, out_file)
        del response
        model.insert(drink,strFile)
        return redirect(url_for('index'))
