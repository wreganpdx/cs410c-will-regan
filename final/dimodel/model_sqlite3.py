"""
A simple Store to Drink database for a flask app.
ata is stored in a SQLite database that looks something like the following:

+------------+---------------
| Drink       | Image    |
+============+================
| Mega Bubble  | drinkImage.png |
+------------+-----------------

This can be created with the following SQL (see bottom of this file):

    create table book (store text, drink text);

"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'image_entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from images_rel")
        except sqlite3.OperationalError:
            cursor.execute("create table images_rel (image text, drink text)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: store, drink
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM images_rel")
        return cursor.fetchall()

    def get_store(self, drink):
        """
        Gets specific store from database
        :return: single tuple from specific store (found by name)
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        search = "SELECT * FROM images_rel where drink ='" + drink+"'"
        cursor.execute(search)
        return cursor.fetchall()

    def insert(self, image, drink):
        """
        Inserts entry into database
        :param image: String
        :param drink: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'image':image, 'drink':drink}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into images_rel (image, drink) VALUES (:image, :drink)", params)

        connection.commit()
        cursor.close()
        return True
