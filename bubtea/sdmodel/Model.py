class Model():
    """
        This is a Store/Drink model database. 
        Basically allowing many to many relationships between drinks.
    """
    def select(self):
        """ 
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def get_store(self, store):
        pass

    def insert(self, store, drink):
        """
        Inserts entry into database
        :param store: String
        :param drink: String
        """
        pass
