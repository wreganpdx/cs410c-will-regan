from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import sdmodel
"""
Add drink will add to a drink-store combination to a relational database.
"""
class Add_Drink(MethodView):
    def get(self, bub):
        return render_template('add_drink.html', bub=bub)

    def post(self, bub):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed.
        """
        model = sdmodel.get_model()
        model.insert(bub,request.form['drink'])
        return redirect(url_for('index'))
