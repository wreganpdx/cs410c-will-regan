"""
A simple guestbook flask app.
ata is stored in a SQLite database that looks something like the following:

+------------+------------------+------------+----------------+
| Name       | Address            | signed_on  | phone        |
+============+==================+============+----------------+
| Bubble Tea  | 123 Main st.      | 2012-05-28 | 555-223-2322 |
+------------+------------------+------------+----------------+

This can be created with the following SQL (see bottom of this file):

    create table book (name text, email text, signed_on date, message);

"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'bt_entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from bubteas")
        except sqlite3.OperationalError:
            cursor.execute("create table bubteas (name text, address text, signed_on date, phone)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, address, date, phone
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM bubteas")
        return cursor.fetchall()

    def get_store(self, store):
        """
        Gets specific store from database
        :return: single tuple from specific store (found by name)
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        search = "SELECT * FROM bubteas where name ='" + store+"'"
        cursor.execute(search)
        return cursor.fetchall()

    def insert(self, name, address, phone):
        """
        Inserts entry into database
        :param name: String
        :param address: String
        :param phone: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'name':name, 'address':address, 'date':date.today(), 'phone':phone}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into bubteas (name, address, signed_on, phone) VALUES (:name, :address, :date, :phone)", params)

        connection.commit()
        cursor.close()
        return True
